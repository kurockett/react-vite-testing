import PostForm from '.';
import userEvent from '@testing-library/user-event';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import { UserEvent } from '@testing-library/user-event/dist/types/setup/setup';

describe('PostForm.tsx', () => {
  let user: UserEvent;
  beforeAll(() => {
    user = userEvent.setup();
  });
  it('should render form', () => {
    render(<PostForm />);
    const form = screen.getByTestId('post-form');
    expect(form).toMatchSnapshot();
    expect(form).toBeDefined();
    expect(form).toBeInTheDocument();
  });
  it('submit form', async () => {
    const handleSubmit = jest.fn();
    render(<PostForm onSubmit={handleSubmit} />);
    const form = screen.getByTestId('post-form');
    await user.type(screen.getByLabelText(/enter username/i), 'John');
    await user.type(screen.getByLabelText(/enter password/i), 'Dee');
    fireEvent.submit(form);
    await waitFor(() => {
      expect(handleSubmit).toBeCalled();
      expect(handleSubmit).toBeCalledWith(form);
    });
  });
});
