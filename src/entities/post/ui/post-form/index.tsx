import { FC } from 'react';
import { getFormData } from '~/shared/helpers';

interface PostFormProps {
  onSubmit?: (smth: any) => void;
}

const PostForm: FC<PostFormProps> = ({ onSubmit }) => {
  const submitForm = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    console.log(getFormData(event.currentTarget));
    if (onSubmit) {
      onSubmit(event.currentTarget);
    }
    event.currentTarget.reset();
  };
  return (
    <form
      onSubmit={submitForm}
      className={'bg-slate-50 shadow-md rounded-xl p-4'}
      data-testid='post-form'
    >
      <fieldset className='mb-4'>
        <label htmlFor='name'>Enter username</label>
        <input
          type='text'
          id='name'
          name='username'
          pattern='\w{3,16}'
          className='border-2  invalid:bg-rose-500 valid:bg-green-500 transition-all delay-75 ease-linear'
          required
        />
      </fieldset>
      <fieldset className='mb-4'>
        <label htmlFor='name'>Enter password</label>
        <input
          type='text'
          id='name'
          name='password'
          pattern='\w{8,16}'
          className='border-2 invalid:bg-rose-500 valid:bg-green-500 transition-all delay-75 ease-linear'
          required
        />
      </fieldset>
      <button
        type='submit'
        className='border-2 bg-purple-900 px-2 py-1 hover:bg-purple-600 transition-all ease-linear delay-150 outline-none rounded-lg text-white'
      >
        Submit form
      </button>
    </form>
  );
};

export default PostForm;
