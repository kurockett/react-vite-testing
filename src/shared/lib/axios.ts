import Axios from 'axios';

const localAxios = Object.assign(Axios, {});

localAxios.interceptors.request.use();

export { localAxios as axios };
