export const getFormData = (form: HTMLFormElement) =>
  Object.fromEntries(new FormData(form));
