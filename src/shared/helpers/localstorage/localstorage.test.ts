import { getAccessToken } from './';

jest.spyOn(Object.getPrototypeOf(localStorage), 'getItem');
jest.spyOn(Object.getPrototypeOf(localStorage), 'setItem');
jest.spyOn(Object.getPrototypeOf(localStorage), 'removeItem');
jest.spyOn(Object.getPrototypeOf(localStorage), 'clear');

describe('localstorage.test', () => {
  beforeEach(() => {
    localStorage.clear();
    jest.clearAllMocks();
  });

  describe('localStorage mock', () => {
    const testId: string = '1';

    beforeEach(() => {
      localStorage.setItem('test-id', testId);
    });

    it('should return', () => {
      const id = localStorage.getItem('test-id');
      expect(id).toBe(testId);
      expect(localStorage.getItem).toBeCalled();
    });

    it('should set', () => {
      localStorage.setItem('test-id', testId);
      expect(localStorage.setItem).toBeCalled();
      expect(localStorage.getItem('test-id')).toBe(testId);
    });

    it('should delete', () => {
      expect(localStorage.getItem('test-id')).toBe(testId);
      localStorage.removeItem('test-id');
      expect(localStorage.removeItem).toBeCalled();
      expect(localStorage.getItem('test-id')).toBeNull();
    });
  });

  describe('getAccessToken', () => {
    const token: string = 'e234234';

    beforeEach(() => {
      localStorage.setItem('access_token', token);
    });

    it('should return token', () => {
      let accessToken = getAccessToken();
      expect(localStorage.getItem).toBeCalled();
      expect(accessToken).toBeDefined();
      expect(accessToken).toBe(token);
    });
  });
});
