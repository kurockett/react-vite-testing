export const getAccessToken = (): string | null =>
  localStorage.getItem('access_token');
