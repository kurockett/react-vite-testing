import { Navbar } from '~/shared/ui';

const Header = () => {
  return (
    <header>
      <h1>Header</h1>
      <Navbar />
    </header>
  );
};

export default Header;
