import { lazy } from 'react';
import { Route, Routes } from 'react-router-dom';
const ErrorPage = lazy(() => import('./error'));
const HomePage = lazy(() => import('./home'));
const PostsPage = lazy(() => import('./post'));

export const Routing = () => {
  return (
    <Routes>
      <Route path='/posts' element={<PostsPage />} />
      <Route index element={<HomePage />} />
      <Route path='*' element={<ErrorPage />} />
    </Routes>
  );
};
