import { PostForm, PostList } from '~/entities/post/ui';
import styles from './styles.module.css';

const PostsPage = () => {
  return (
    <section className={styles.container}>
      <div>
        <PostForm />
      </div>
      <div className={styles.content}>
        <PostList />
      </div>
    </section>
  );
};

export default PostsPage;
